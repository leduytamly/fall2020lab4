/* Le Duytam Ly
 * 1734119*/
package bookstore;

public class Book {
	protected String title;
	private String author;
	
	/**
	 * Getter for title
	 * @return the title of the book
	 */
	public String getTitle() {
		return this.title;
	}
	
	/**
	 * Getter for author
	 * @return the author of the book
	 */
	public String getAuthor() {
		return this.author;
	}
	
	/**
	 * Parameterized Constructor
	 * @param title Title of the book
	 * @param author Author of the book
	 */
	public Book(String title, String author) {
		this.title = title;
		this.author = author;
	}
	
	@Override
	public String toString() {
		return ("Title: "+title+", author: "+author);
	}
	
}
