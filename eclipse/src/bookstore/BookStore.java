/* Le Duytam Ly
 * 1734119*/
package bookstore;

public class BookStore {
	public static void main(String[] args) {
		Book[] books = new Book[5];
		books[0]= new Book("1984","George Orwell");
		books[1]= new ElectronicBook("Brave New World", "Aldous Huxley",5000);
		books[2]= new Book("Fahrenheit 451","Ray Bradbury");
		books[3]= new ElectronicBook("Oryx and Crake","Margaret Atwood",60000);
		books[4]= new ElectronicBook("Animal Farm","George Orwell",2000);
		
		printBooks(books);
	}
	
	/**
	 * Prints all Book in a books array
	 * @param books Book array
	 */
	public static void printBooks(Book[] books) {
		for (Book b : books ) {
			System.out.println(b.toString());
		}
	}
}
