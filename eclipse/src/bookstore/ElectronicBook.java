/* Le Duytam Ly
 * 1734119*/
package bookstore;

public class ElectronicBook extends Book{
	private double numberBytes;
	
	/**
	 * Parameterized Constructor
	 * @param title Title of the book
	 * @param author Author of the book
	 * @param numberBytes Number of bytes the book takes
	 */
	public ElectronicBook(String title, String author, double numberBytes) {
		super(title,author);
		if (numberBytes<0) {
			throw new IllegalArgumentException("number cant be negative");
		}
		this.numberBytes=numberBytes;
	}
	
	 @Override
	public String toString() {
		String fromBase = super.toString();
		fromBase = fromBase + ", number of bytes: "+numberBytes;
		return fromBase;
	}
}
