/* Le Duytam Ly
 * 1734119*/
package geometry;

public class Circle implements Shape {
	private double radius;
	
	/**
	 * Parameterized Constructor
	 * @param radius Radius of the circle
	 */
	public Circle(double radius) {
		if (radius < 0) {
			throw new IllegalArgumentException("radius cannot be negative");
		}
		this.radius = radius;
	}
	
	/**
	 * Getter for radius
	 * @return radius
	 */
	public double getRadius() {
		return this.radius;
	}

	/**
	 * Calculates the area of the circle
	 */
	public double getArea() {
		double area = Math.PI * this.radius*this.radius;
		return area;
	}
	
	/**
	 * Calculates the perimeter of the circle
	 */
	public double getPerimeter() {
		double perimeter = 2*Math.PI*this.radius;
		return perimeter;
	}
}
