/* Le Duytam Ly
 * 1734119*/
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(2,4);
		shapes[1] = new Rectangle(30,3);
		shapes[2] = new Circle(5);
		shapes[3]= new Circle(14);
		shapes[4] = new Square(3);
		
		printShapes(shapes);
	}
	
	/**
	 * Prints all shapes in the array
	 * @param shapes Shape array
	 */
	public static void printShapes(Shape[] shapes) {
		for (Shape s : shapes) {
			System.out.println(s.getClass().getSimpleName()+": Area is "+s.getArea()+" and Perimeter is "+s.getPerimeter());
		}
	}

}
