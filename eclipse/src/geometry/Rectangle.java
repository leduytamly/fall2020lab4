/* Le Duytam Ly
 * 1734119*/
package geometry;

public class Rectangle implements Shape{
	private double length;
	private double width;
	
	/**
	 * Parameterized Constructor
	 * @param length Length of the rectangle
	 * @param width Width of the rectangle
	 */
	public Rectangle(double length, double width) {
		if(length<0 || width<0) {
			throw new IllegalArgumentException("length or width cannot be negative");
		}
		this.length=length;
		this.width=width;
	}
	
	/**
	 * Getter for Length
	 * @return length of the rectangle
	 */
	public double getLength() {
		return this.length;
	}
	
	/**
	 * Getter for width
	 * @return length of the rectangle
	 */
	public double getWidth() {
		return this.width;
	}

	/**
	 * Calculates the area of the rectangle
	 */
	public double getArea() {
		double area = width*length;
		return area;
	}
	
	/**
	 * Calculates the perimeter of the rectangle
	 */
	public double getPerimeter() {
		double perimeter = 2*width+2*length;
		return perimeter;
	}
}
