/* Le Duytam Ly
 * 1734119*/
package geometry;

public interface Shape {
	double getArea();
	double getPerimeter();
}
