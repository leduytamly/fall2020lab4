/* Le Duytam Ly
 * 1734119*/
package geometry;

public class Square extends Rectangle{
	
	/**
	 * Parameterized Constructor
	 * @param sideLength side of the square
	 */
	public Square(double sideLength) {
		super(sideLength,sideLength);
	}
}
